import { useEffect, useState } from 'react'
import SearchMenu from './components/searchmenu/SearchMenu';
import FilterSection from './components/filter-section/FilterSection';
import SearchedBar from './components/searched-bar/SearchedBar';
import CardGrid from './layouts/card-grid/CardGrid';
import StatsCard from './components/cards/stats-card/StatsCard';
import Dashboard from './pages/dashboard/Dashboard';
function App() {
  useEffect(()=>{
    const wrapper = document.querySelector(".wrapper");
    const header = document.querySelector(".header");
    
    wrapper.addEventListener("scroll", (e) => {
     e.target.scrollTop > 30
      ? header.classList.add("header-shadow")
      : header.classList.remove("header-shadow");
    });
    
    const toggleButton = document.querySelector(".dark-light");
    
    toggleButton.addEventListener("click", () => {
     document.body.classList.toggle("dark-mode");
    });
    
    const jobCards = document.querySelectorAll(".job-card");
    const logo = document.querySelector(".logo");
    const jobLogos = document.querySelector(".job-logos");
    const jobDetailTitle = document.querySelector(
     ".job-explain-content .job-card-title"
    );
    const jobBg = document.querySelector(".job-bg");
    
    jobCards.forEach((jobCard) => {
     jobCard.addEventListener("click", () => {
      const number = Math.floor(Math.random() * 10);
      const url = `https://unsplash.it/640/425?image=${number}`;
      jobBg.src = url;
    
      const logo = jobCard.querySelector("img");
      const bg = logo.style.backgroundColor;
      console.log(bg);
      jobBg.style.background = bg;
      const title = jobCard.querySelector(".job-card-title");
      jobDetailTitle.textContent = title.textContent;
      jobLogos.innerHTML = logo.outerHTML;
      wrapper.classList.add("detail-page");
      wrapper.scrollTop = 0;
     });
    });
    
    logo.addEventListener("click", () => {
     wrapper.classList.remove("detail-page");
     wrapper.scrollTop = 0;
       jobBg.style.background = bg;
    });
  
  },[])
  
  return (
    <div class="job">
    <div class="header">
     <div class="logo">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
       <path xmlns="http://www.w3.org/2000/svg" d="M512 503.5H381.7a48 48 0 01-45.3-32.1L265 268.1l-9-25.5 2.7-124.6L338.2 8.5l23.5 67.1L512 503.5z" fill="#0473ff" data-original="#28b446" />
       <path xmlns="http://www.w3.org/2000/svg" fill="#0473ff" data-original="#219b38" d="M361.7 75.6L265 268.1l-9-25.5 2.7-124.6L338.2 8.5z" />
       <path xmlns="http://www.w3.org/2000/svg" d="M338.2 8.5l-82.2 234-80.4 228.9a48 48 0 01-45.3 32.1H0l173.8-495h164.4z" fill="#0473ff" data-original="#518ef8" />
      </svg>
      Wealzy
     </div>
     <div class="header-menu">
      <a href="#" class="active">Find Job</a>
      <a href="#">Company Review</a>
      <a href="#">Find Salaries</a>
     </div>
     <div class="user-settings">
      <div class="dark-light">
       <svg viewBox="0 0 24 24" stroke="currentColor" stroke-width="1.5" fill="none" stroke-linecap="round" stroke-linejoin="round">
        <path d="M21 12.79A9 9 0 1111.21 3 7 7 0 0021 12.79z" /></svg>
      </div>
      <div class="user-menu">
       <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-square">
        <rect x="3" y="3" width="18" height="18" rx="2" ry="2" /></svg>
      </div>
      <img class="user-profile" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3364143/download+%283%29+%281%29.png" alt=""/>
      <div class="user-name">Suhayel Nasim</div>
     </div>
    </div>
    <div class="wrapper ">
    {/* <StatsCard/> */}


    <SearchMenu/>
     <div class="main-container">
     <FilterSection/>
    <CardGrid/>
    
    {/* <Dashboard/> */}
  
     </div>
    </div>
   </div>
  )
}

export default App
