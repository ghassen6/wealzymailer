import React from 'react'

const StatsCard = () => {
  return (
    <div>

        <div class="row stats" style={{display:'flex',justifyContent:'space-around'}}>

<div class="col_stat">
  <div class="statContainer yellow shadow-sm">
    <div class="d-flex">
      <div class="p-2 flex-fill text-center status">Total <br/>
        <h5 class="ont-weight-bold">20</h5>
      </div>
    </div>
  </div>
</div>
<div class="col_stat">
  <div class="statContainer yellow shadow-sm">
    <div class="d-flex">
      <div class="p-2 flex-fill text-center status">Envoyer <br/>
        <h5 class="ont-weight-bold">20</h5>
      </div>
    </div>
  </div>
</div>
<div class="col_stat">
  <div class="statContainer yellow shadow-sm">
    <div class="d-flex">
      <div class="p-2 flex-fill text-center status">erroné <br/>
        <h5 class="ont-weight-bold">20</h5>
      </div>
    </div>
  </div>
</div>
<div class="col_stat">
  <div class="statContainer yellow shadow-sm">
    <div class="d-flex">
      <div class="p-2 flex-fill text-center status">ignorer <br/>
        <h5 class="ont-weight-bold">20</h5>
      </div>
    </div>
  </div>
</div>
</div>
</div>

  )
}
// create app 

export default StatsCard