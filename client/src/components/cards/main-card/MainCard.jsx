import React from 'react'

const MainCard = ({title,description,tags,actions}) => {
  return (
    <div class="job-card">
         <div class="job-card-header">
         
          <img src='https://application.wealzy.com/static/media/logo_vertical_haut_bleu_texte_bleu.52e0bf02.png' alt="logowealzy"/>
          <div class="menu-dot"></div>
         </div>
         <div class="job-card-title">{title}</div>
        {description &&
         <div class="job-card-subtitle">
Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi nisi cum molestias doloremque exercitationem dolore ipsam porro dolor eaque libero atque, fugit odio nobis, recusandae ullam beatae tenetur, aliquid sequi.         
         </div>
         }
{  tags&&  <div class="job-detail-buttons">
          <button class="search-buttons detail-button">Full Time</button>
          <button class="search-buttons detail-button">Min. 1 Year</button>
          <button class="search-buttons detail-button">Senior Level</button>
         </div>
} {actions &&
         
 <div class="job-card-buttons">
                  <button class="search-buttons card-buttons">Apply Now</button>
          <button class="search-buttons card-buttons-msg">Messages</button>
          </div>
                 }

        </div>  )
}

export default MainCard