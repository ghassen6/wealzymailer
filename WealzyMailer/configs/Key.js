const { database, databaseUser, databasePassword } = require("../config");

const mongoURI =
  "mongodb+srv://" +
  databaseUser +
  ":" +
  databasePassword +
  "@wealzy.cbzbn.gcp.mongodb.net/?useNewUrlParser=true&w=majority";
const localdevURI = "mongodb://localhost:27017/wealzylocalDev";
module.exports = {
  mongoURI: mongoURI,
  db_connect_options: {
    useNewUrlParser: true,
    retryWrites: true,
    w: "majority",
    useUnifiedTopology: true,
    user: databaseUser,
    pass: databasePassword,
    dbName: database,
  },
  secretOrKey: "secret",
};
