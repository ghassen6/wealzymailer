const {FetchCritGrouppedByUser,CovertAllCritere} =require("../repos/critere/critere.repo")
const {SendAllMails} = require("../repos/mail/mail.repo")
const NodeRedisClient = require('../repos/redis/redis-client');
const nodeRedisClient = new NodeRedisClient();

const SendMails=async()=>{
console.info("Send Emails")    
const CritList = await FetchCritGrouppedByUser(); 
const FinalDetailedList=await CovertAllCritere(CritList);
const MailResult =  await SendAllMails(FinalDetailedList)
try{
await nodeRedisClient.set('MailResult', JSON.stringify(MailResult));

}catch(e){
    console.log(e.message)
}
}
module.exports={
    SendMails 
}