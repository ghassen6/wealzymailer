const nodemailer = require("nodemailer");
const hbs = require("nodemailer-express-handlebars");
const path = require("path");
const mailService = require("./email-sender");
const smtpTransport = nodemailer.createTransport({
  service: "Gmail",
  pool: true, // This is the field you need to add

  auth: {
    user: process.env.REACT_APP_MAIL, // adresse Wealzy (mettre dans le env)
    pass: process.env.REACT_APP_MAIL_PWD, //mettre le mot de passe dans env
  },
});
const SendAllMails = async (MailsArray) => {
  let GloabalMailStatus = {
    success: 0,
    failed: 0,
    ignored: 0,
  };



  for (let Mail of MailsArray) {
    let MailSent = await SendMails(
      Mail.userid,
      Mail.user,
      Mail.nom_critere,
      Mail.data,
      Mail.critere
    );
    if (MailSent !== undefined) {
      if (MailSent.status == "failed") {
        Mail.mailStatus = "failed";
        Mail.failIndicator = MailSent.message;
        GloabalMailStatus.failed++;
      } else if (MailSent.status == "success") {
        Mail.mailStatus = "success";
        Mail.failIndicator = "empty";
        GloabalMailStatus.success++;
      } else {
        Mail.mailStatus = "ignored";
        Mail.failIndicator = "empty";
        GloabalMailStatus.ignored++;
      }
    }
  }
  console.table(
    MailsArray.map((element) => {
      return {
        email: element.email,
        "nom critere": element.nom_critere,
        "nombre de biens": element.data.length,
      };
    })
  );
  return({ data: MailsArray, status: GloabalMailStatus });
};
const SendMails = async (iduser, user, nom, biens, critere) => {
  if (biens.length > 0) {
    let mailOptions = {
      to: "ghassen@wealzy.com", //user.email,//"ghassen@wealzy.com",//
      from: process.env.REACT_APP_MAIL,
      subject: "Critere " + nom + " " + biens.length + " nouvelles annonces",
      template: "index",
      context: {
        loc: "resid",
        type: critere.type_bien != undefined ? critere.type_bien["$in"] : "",
        nom_commune:
          critere.code_postal && biens[0].nom_commune != undefined
            ? biens[0].nom_commune + " " + biens[0].code_postal
            : "",
        surface:
          "Surface entre " +
          (critere?.surface?.["$gte"] || 0) +
          " et " +
          (critere?.surface?.["$lte"] || 2000000),
        prix_bien:
          "Prix bien entre " +
          (critere?.prix_bien?.["$gte"] || 0) +
          " et " +
          (critere?.prix_bien?.["$lte"] || 2000000),
        nbe_piece:
          "Nombre de piéces entre " +
          (critere?.nbe_piece?.["$gte"] || 0) +
          " et " +
          (critere?.nbe_piece?.["$lte"] || 2000000),
        bien: biens,
        newbien: biens.length + " ",
        user: nom || "Default User Name",
        usern: iduser || "Default User ID",
        critere: JSON.stringify(critere) || "{}",
      },
    };

    smtpTransport.use(
      "compile",
      hbs({
        viewEngine: {
          extName: ".handlebars",
          partialsDir: path.join(__dirname),
          defaultLayout: false,
        },
        viewPath: path.join(__dirname),
        extName: ".handlebars",
      })
    );

    let ServiceMail = new mailService(smtpTransport);

    let result = await ServiceMail.sendMail(mailOptions);

    return result;
  } else {
    return { status: "ignored" };
  }
};

module.exports = { SendAllMails };
