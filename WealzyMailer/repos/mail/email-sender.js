class MailService {
    constructor(transporter) {
      this.transporter = transporter;
    }
  
    async sendMail(mailOptions) {
      try {
        const info = await this.transporter.sendMail(mailOptions);
        console.log('Mail sent successfully:', info.messageId);
        if(info.accepted.length > 0){
          return {status:"success"}
        }
        
        
      } catch (error) {
        console.error('Error sending mail:', error);
        return {status:'failed',message:error.message}
  
      }
    }
  }
  
  module.exports = MailService;
  