const Criteria = require("../../models/CriteresRecherche");
const { FetchBienByCritere } = require("../annonce/annonce.repo");

//Fetch Critere Groupped By User
const dateh = new Date();
dateh.setDate(dateh.getDate() - 2);
dateh.setHours(23, 0, 0, 0);

const FetchCritGrouppedByUser = async () => {
  const CritList = await Criteria.aggregate([
    {
      $lookup: {
        from: "users",
        localField: "user",
        foreignField: "_id",
        as: "user",
      },
    },
    {
      $unwind: "$user",
    },
    {
      $match: {
        notification: true,
      },
    },
    {
      $group: {
        _id: "$user.email",
        criteria: {
          $push: "$$ROOT",
        },
      },
    },
  ]).exec();

  return CritList;
};
//Fetch Convert Critere

const ConvertCritere = async (crit, dateh) => {
  const arrayCritere = await Promise.all(
    crit.map(async (element) => {
      const temp = {
        date_extraction: { $gte: dateh },
        etat_annonce: 0,
      };

      if (element.departement_cible?.length > 0) {
        temp.code_departement = { $in: element.departement_cible };
      }

      const codeInsee =
        element.commune_cible?.map((commune) => commune?.value?.code_insee) ||
        [];
      const code_postal =
        element.commune_cible?.flatMap(
          (commune) => commune?.value?.code_postal
        ) || [];
      if (codeInsee.length > 0) {
        temp.code_insee = { $in: codeInsee };
      }
      if (code_postal.length > 0) {
        temp.code_postal = { $in: code_postal };
      }

      if (element.surface_min || element.surface_max) {
        temp.surface = {
          $gte: element.surface_min || 0,
          $lte: element.surface_max || Infinity,
        };
      }

      if (element.prix_m2_min || element.prix_m2_max) {
        temp.prix_m2 = {
          $gte: element.prix_m2_min || 0,
          $lte: element.prix_m2_max || 15000,
        };
      }

      if (element.nb_etage_min || element.nb_etage_max) {
        temp.nbe_etage = {
          $gte: element.nb_etage_min || 0,
          $lte: element.nb_etage_max || 10,
        };
      }

      if (element.nbe_piece_min || element.nbe_piece_max) {
        temp.nbe_piece = {
          $gte: element.nbe_piece_min || 0,
          $lte: element.nbe_piece_max || Infinity,
        };
      }

      if (element.budget_min || element.budget_max) {
        temp.prix_bien = {
          $gte: element.budget_min || 0,
          $lte: element.budget_max || Infinity,
        };
      }

      if (
        element.type_bien?.length > 0 &&
        typeof element.type_bien !== "undefined"
      ) {
        temp.type_bien_corrige = { $in: element.type_bien };
      }

      return temp;
    })
  );
  return arrayCritere;
};

const CovertAllCritere = async (criteria) => {
  const MailobjectArray = [];
  for (const element of criteria) {
    const Element = element;

    let ConvertedCritList = [];
    try {
      ConvertedCritList = await ConvertCritere(
        Element.criteria,
        dateh.toISOString()
      );
    } catch (error) {
      console.error(error);
    }

    for (let j = 0; j < ConvertedCritList.length; j++) {
  
      let Mailobject = {
        nom_critere: Element.criteria[j].nom_critere,
        email: Element.criteria[j].user.email,
        userid:Element.criteria[j].user._id,
        user:Element.criteria[j].user,
        critere:ConvertedCritList[j],
        data: [],
      };
      const biens = await FetchBienByCritere(ConvertedCritList[j]);

       Mailobject.data=biens

      biens.length>0&&MailobjectArray.push(Mailobject);

    }
  }


  return MailobjectArray;
};

module.exports = {
  FetchCritGrouppedByUser,
  CovertAllCritere,
};
