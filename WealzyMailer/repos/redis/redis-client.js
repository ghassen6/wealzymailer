const util = require('util');

class NodeRedisClient  {
    constructor() {
      this.client = require('redis').createClient({
        legacyMode: true,
        socket: {
          host: "redis",
          port: process.env.REDIS_PORT || 6379,
        },
      });
      this.client.connect()

      // Define getAsync as an instance variable
      this.getAsync = util.promisify(this.client.get).bind(this.client);

      this.client.on('connect', async() => {
        this.client.sendCommand('config', ['set', 'notify-keyspace-events', 'KEA']);
        console.log('✔️ Redis client is connected');
           
      });
      this.client.on('error', (err) => {
        console.error('Redis client connection failed:', err);
      });
    }
  
    async get(key) {
      const value = await this.getAsync(key);
      return value;
    }    

    async set(key, value) {
      return new Promise((resolve, reject) => {
        this.client.set(key, value, (err, reply) => {
          if (err) return reject(err);
          resolve(reply);
        });
      });
    }

    async del(key) {
      return new Promise((resolve, reject) => {
        this.client.del(key, (err, reply) => {
          if (err) return reject(err);
          resolve(reply);
        });
      });
    }
}

module.exports = NodeRedisClient;
