const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const BienSchema = new Schema(
  {
    etage: {
      type: Number,
      default: 0,
    },
    nbe_etage: {
      type: Number,
      default: 0,
    },
    id_db: {
      type: String,
    },
    id_annonce: {
      type: String,
    },
    url_annonce: {
      type: String,
    },
    nom_agence: {
      type: String,
    },
    tel_agence: {
      type: String,
    },
    email_agence: {
      type: String,
    },
    commentaire: {
      type: String,
    },
    code_postal: {
      type: String,
    },
    code_insee: {
      type: String,
    },
    nom_commune: {
      type: String,
    },
    type_bien: {
      type: String,
    },
    type_bien_corrige: {
      type: String,
    },
    travaux: {
      type: Boolean,
    },
    description_bien: {
      type: String,
    },
    code_departement: {
      type: String,
    },
    source: {
      type: String,
    },
    date_extraction: {
      type: String,
    },
    prix_bien: {
      type: Number,
    },
    ancien_prix: {
      type: Array,
    },
    surface: {
      type: Number,
    },
    nbe_piece: {
      type: Number,
    },
    nbe_chambre: {
      type: Number,
    },
    annonceorbien: {
      type: Number,
      default: 0,
    },
    latitude: {
      type: Number,
    },
    longitude: {
      type: Number,
    },
    adresse: {
      type: String,
    },
    nom_iris: {
      type: String,
    },
    code_iris: {
      type: String,
    },
    url_photo: {
      type: Array,
    },
    favoris_photo: {
      type: Array,
    },
    type_offer: {
      type: String,
    },
    qualification_robot: {
      statut_qualification: {
        type: String,
      },
      clients: {
        type: Array,
      },
      vente: {
        prix_bien_estime_notaire: {
          type: Number,
        },
        prix_bien_estime: {
          type: Number,
        },
        prix_m2_vente_wealzy_moyen: {
          type: Number,
        },
        prix_m2_vente_notaire_moyen: {
          type: Number,
        },
        plus_value_brut: {
          type: Number,
        },
        prix_m2_vente_reel: {
          type: Number,
        },
        plus_value_net: {
          type: Number,
        },
      },
      rendement: {
        cash_flow_moyen: {
          type: Number,
        },
        rendement_net_retenue: {
          type: Number,
        },
        rendement_brut_retenue: {
          type: Number,
        },
        rendement_net_location_nue: {
          type: Number,
        },
        rendement_net_colocation_nue: {
          type: Number,
        },
        rendement_brut_colocation: {
          type: Number,
        },
        rendement_brut_location_meuble: {
          type: Number,
        },
        rendement_brut_location_nue: {
          type: Number,
        },
        rendement_net_location_meuble: {
          type: Number,
        },
      },
      prix: {
        prix_bien_fai: {
          type: Number,
        },
        frais_agence: {
          type: Number,
        },
        frais_notaire: {
          type: Number,
        },
        commission_wealzy: {
          type: Number,
        },
        montant_travaux: {
          type: Number,
        },
        montant_meuble: {
          type: Number,
        },
        autres_frais: {
          type: Number,
        },
        prix_bien_hors_fai: {
          type: Number,
        },
        negociation: {
          type: Number,
        },
        prix_bien_complet: {
          type: Number,
        },
      },
      charge: {
        entretien_courant: {
          type: Number,
        },
        charges_copropriete: {
          type: Number,
        },
        charges_complementaires: {
          type: Number,
        },
        garantie_impayes: {
          type: Number,
        },
        assurance_pno: {
          type: Number,
        },
        taxe_fonciere: {
          type: Number,
        },
        charges_gestion: {
          type: Number,
        },
        total_charges: {
          type: Number,
        },
      },
      location: {
        prix_location_nue_mensuel: {
          type: Number,
        },
        prix_location_nue_annuel: {
          type: Number,
        },
        prix_location_meublee_mensuel: {
          type: Number,
        },
        prix_location_meublee_annuel: {
          type: Number,
        },
        eligible_colocation: {
          type: Number,
        },
        prix_colocation_chambre_mensuel: {
          type: Number,
        },
        prix_colocation_mensuel: {
          type: Number,
        },
        prix_colocation_annuel: {
          type: Number,
        },
        option_location: {
          type: Number,
        },
        prix_location_retenue_mensuel: {
          type: Number,
        },
        prix_location_retenue_annuel: {
          type: Number,
        },
        prix_m2_location_moyen: {
          type: Number,
        },
        nbeChambre: {
          type: Number,
        },

      },
      financement: {
        apport_personnel: {
          type: Number,
        },
        montant_a_emprunter: {
          type: Number,
        },
        nbe_mensualite_remboursement: {
          type: Number,
        },
        taux_emprunt: {
          type: Number,
        },
        taux_assurance: {
          type: Number,
        },
        frais_courtage: {
          type: Number,
        },
        frais_dossier_financement: {
          type: Number,
        },
        frais_garanties_cuationnement: {
          type: Number,
        },
        montant_mensualite_remboursement: {
          type: Number,
        },
        montant_mensualite_hors_assurance: {
          type: Number,
        },
        montant_mensualite_assurance: {
          type: Number,
        },
        tableau_amortissement_pret: {
          type: Array,
        },
      },
      fiscalite: {
        option_fiscale: {
          type: Number,
        },
        revenu_foncier_annuel: {
          type: Number,
        },
        tableau_charges_interets_annuel: {
          type: Array,
        },
        charges_interets_moyen_5_ans: {
          type: Number,
        },
        nbe_annee_deficit_foncier: {
          type: Number,
        },
        montant_amortissement_bien_annuel: {
          type: Number,
        },
      },
    },
  },
  { strict: false, collection: "annonce_qualification" }
);
const Bien = mongoose.model("bien", BienSchema);
module.exports = Bien;
