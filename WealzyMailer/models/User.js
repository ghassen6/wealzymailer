/** @format */

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
  nom: {
    type: String,
  },
  prenom: {
    type: String,
  },
  telephone: {
    type: String,
  },
  birthday: {
    type: String,
  },
  adresse: {
    type: String,
  },
  email: {
    type: String,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: Number,
    required: true,
  },
  admin_gestionnaire: {
    type: String,
  },
  gestionnaire: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  dateVal: {
    type: Date,
    default: Date.now,
  },
  adresse: {
    type: String,
  },
  etatBien: {
    type: String,
  },
  departement_cible: {
    type: Array,
  },
  commune_cible: {
    type: Array,
  },
  nbe_piece: {
    type: Array,
    default: [0, 10],
  },
  rendement: {
    type: Array,
  },
  surface: {
    type: Array,
    default: [0, 2000000],
  },

  plus_value: {
    type: Array,
  },
  budget: {
    type: Array,
    default: [0, 2000000],
  },
  prix_m2: {
    type: Array,
    default: [0, 15000],
  },
  nbe_etage: {
    type: Array,
    default: [0, 10],
  },
  type_bien: {
    type: Array,
  },
  caract_bien: {
    type: Array,
  },
  commentaire: {
    type: String,
  },
  raison_sociale: {
    type: String,
  },
  siret: {
    type: String,
  },
  telephone_entreprise: {
    type: String,
  },
  adresse_entreprise: {
    type: String,
  },
  siteweb_entreprise: {
    type: String,
  },
  email_entreprise: {
    type: String,
  },
  logo_entreprise: {
    type: String,
  },
  statut_client: {
    type: String,
  },
  statut_sprint: {
    type: String,
  },
  date_sprint: {
    type: Date,
  },
  date_fin_sprint: {
    type: Date,
  },
  bien_propose: {
    type: Array,
  },
  IDStripe: {
    type: String,
  },
  id_sub: {
    type: String,
  },
  idproduct: {
    type: String,
  },
  mailVerified: {
    type: Boolean,
  },
  Alert_Activer: {
    type: Boolean,
    default: true,
  },
  isLoggedIn: {
    type: Boolean,
    default: false,
  },
  lastConnexion: {
    type: Date,
  },
  resetPasswordToken: {
    type: String,
  },
  resetPasswordExpires: {
    type: Date,
  },
  billing_details: {
    type: Object,
  },
  couleurprimaire: {
    type: String,
  },
  couleursecondaire: {
    type: String,
  },
  ipAddress: {
    type: String,
  },
  browserVersion: {
    type: String,
  },
  browserOs: {
    type: String,
  },
  browserName: {
    type: String,
  },
  lastC: {
    type: Boolean,
    default: false,
  },
  list_visited: {
    type: Array,
  },
  id_subscription_charge_bee: {
    type: String,
  },
  id_customer_charge_bee: {
    type: String,
  },

  expires_at: {},
});
const User = mongoose.model("users", UserSchema);
module.exports = User;
