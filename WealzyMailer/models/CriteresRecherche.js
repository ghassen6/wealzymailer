const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const CriteresRecherche = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
  },
  nom_critere: {
    type: String,
    trim: true,
  },

  type_cible: {
    type: String,
    default: "locatif",
  },
  keyword: {
    type: String,
  },
  departement_cible: {
    type: Array,
  },
  commune_cible: {
    type: Array,
  },
  nbe_piece_min: {
    type: Number,
    default: 0,
  },
  nbe_piece_max: {
    type: Number,
    default: 10,
  },
  rendement_min: {
    type: Number,
    default: 0,
  },
  rendement_max: {
    type: Number,
    default: 30,
  },
  surface_min: {
    type: Number,
    default: 0,
  },
  surface_max: {
    type: Number,
    default: 2000000,
  },

  plus_value_min: {
    type: Number,
    default: 0,
  },
  plus_value_max: {
    type: Number,
    default: 1000000,
  },

  budget_min: {
    type: Number,
    default: 0,
  },
  budget_max: {
    type: Number,
    default: 2000000,
  },
  notification: {
    type: Boolean,
    default: false,
  },
  prix_m2_min: {
    type: Number,
    default: 0,
  },
  prix_m2_max: {
    type: Number,
    default: 15000,
  },

  nb_etage_min: {
    type: Number,
    default: 0,
  },

  nb_etage_max: {
    type: Number,
    default: 10,
  },

  type_bien: {
    type: Array,
  },
  caract_bien: {
    type: Array,
  },
  travaux: {
    type: String,
    default: "",
  },
  type_annonce: {
    type: String,
    default: "",
  },
  commentaire: {
    type: String,
  },
});
const criteres = mongoose.model("criteresrecherches", CriteresRecherche);
module.exports = criteres;
