/** @format */

const Router = require('express');
const Logs = require('./log.routes');
const routerloader = () => {
  const app = Router();
  Logs(app);
  return app;
};

module.exports = routerloader;
