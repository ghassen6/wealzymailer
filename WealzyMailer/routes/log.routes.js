/** @format */

const express = require('express');
const logController = require('../controller/logger.controller')
const route = express.Router();

const userRoute = (app) => {
  app.use('/logs', route);

  /**
   * @fetch_users
   */
  route.get('/fetchlogs', logController.FetchLogs);

 
};
module.exports = userRoute;
