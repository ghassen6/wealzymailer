const NodeRedisClient = require('../repos/redis/redis-client');


const groupData = async(data)=>{
  return  data.reduce((acc, obj) => {
        const email = obj.email;
        // const critdata = { "nom_critere": obj.nom_critere, "data": obj };
        const foundIndex = acc.findIndex(x => x.user === email);
        if (foundIndex === -1) {
          acc.push({ "user": email, "critdata": [obj] });
        } else {
          acc[foundIndex]["critdata"].push(obj);
        }
        return acc;
      }, []);
}
const FetchLogs = async (req,res)=>{
    console.log("fetchlogs")
    const nodeRedisClient =  new NodeRedisClient();

    let LogsList =await  nodeRedisClient.get('MailResult')
    LogsList = JSON.parse(LogsList)
    LogsList.data = await groupData(LogsList.data)
    res.send(LogsList?LogsList:"nothing").status(200);


}

module.exports={FetchLogs}