/** @format */

const express = require("express");
const config = require("./config");

async function startServer() {
 

  const app = express();
;
  await require("./loader")(app);
  app
    .listen(config.port, () => {
      console.log(`✔️  Server listening`);
      console.log(`✔️  port : ${config.port}`);
    })
    .on("error", (err) => {
      console.error(err);
      process.exit(1);
    });
}
startServer();
