/** @format */

const express = require('express');
const cors = require('cors');
const routes = require('../routes');
const bodyParser = require("body-parser");

const expressloader = (app) => {
  app.use(cors());
  app.use(express.json());
  app.use(bodyParser.json());
  app.use(express.urlencoded({ extended: true }));
  app.use('/mail', routes());
};

module.exports = expressloader;
