/** @format */

const mongoose = require('mongoose');
const db = require('../configs/Key').mongoURI;
const db_connect_options = require('../configs/Key').db_connect_options;

const mongooseloader = async () => {
  const connection = await mongoose.connect(db, db_connect_options);

  return connection.connection.db;
};
module.exports = mongooseloader;
