/** @format */

const expressLoader = require('./express');
const mongooseLoader = require('./mongoose');
const {SendMails} = require("../services/mail.service")

const SendEmails = async () =>{
  await SendMails();
}


const loader = async (expressApp) => {
  const mongoConnection = await mongooseLoader();
  console.info('✔️  DB loaded and connected!');
   expressLoader(expressApp);
  console.info('✔️  Express loaded');
  SendEmails();
};
module.exports = loader;
