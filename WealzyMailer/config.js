const dotenv = require("dotenv");

dotenv.config();

let port=7013;


module.exports = {
  environnement: process.env.NODE_ENV,
  port: port,
  database:
    process.env.NODE_ENV === "production" ||
    process.env.NODE_ENV === "preproduction" ||
    process.env.NODE_ENV === "lab"
      ? "wealzy_production"
      : "wealzy_development",
  databaseUser: process.env.MONGODB_USER,
  databasePassword: process.env.MONGODB_PASSWORD,
};

